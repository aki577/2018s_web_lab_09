package ictgradschool.web.lab09.ex03;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticlePostServlet extends HttpServlet
    {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
        response.setContentType("text/html");

        String aT = request.getParameter("aTitle");
        PrintWriter out = response.getWriter();
        out.println("<h1>" + aT + "</h1>");
        out.println();

        String aA = request.getParameter("aAuthor");
        out.print("<p>by " + aA + "</p>");
        out.println();
        String g = request.getParameter("genre");
        out.println("<p>Genre: "+g+"</p>");
        out.println();

        String content = request.getParameter("content");
        out.println("<p>"+content+"</p>");
        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
        }
    }
