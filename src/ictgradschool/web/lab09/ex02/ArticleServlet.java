package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet
    {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {

        }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
        {
        response.setContentType("text/html");

        String aT = request.getParameter("aTitle");
        PrintWriter out = response.getWriter();
        out.println("<h1>" + aT + "</h1>");
        out.println();

        String aA = request.getParameter("aAuthor");
        out.print("<p>by " + aA + "</p>");
        String g = request.getParameter("genre");
        out.println("<p>Genre: "+g+"</p>");
        out.println();

        String content = request.getParameter("content");
        out.println("<p>"+content+"</p>");
        out.println();

        out.println("<h2>This is a string array from Java</h2>");
        out.print("<ul>");
        String[] str = new String[4] ;
        String s = "";
        for(int i = 0 ; i< 4 ; i++){
            switch (i){
                case 0:
                    s="Four";
                    break;
                    case 1:
                    s="Three";
                    break;
                case 2:
                    s="Two";
                    break;
                case 3:
                    s="One";
                    break;
            }
            str[i] = "<li>"+s+"</li>";
            out.print(str[i]);
        }
        out.println("</ul>");
        }
    }
